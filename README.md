# Stripe

Passerelle Stripe pour les devis et paiement de l'Université des colibris

## Spécifications paiement UCPO

1. Depuis le site de l'université
2. Si on est connecté un bouton  "Passer commande"  sinon demande de s'identifier
3. Test de l'email : si email connu de stripe, charger la fiche client et préremplir le formulaire, sinon Formulaire de création du compte avec infos sso
4. Données demandées dans le formulaire
<message explicatif pour les orga ou les particuliers>
Organisme (facultatif)
Prénom Nom (contact nom prénom depuis le sso) *
Email (depuis le sso) *
Pays (FR) *
Adresse 1 *
Adresse 2
Code postal *
Ville *
Tarifs choix entre 2 tarifs "plein tarif" ou "tarif solidaire (étudiants, RSA, demandeur d'emploi)"
Nombre de participants * (slider de 1 à 10, 1 par défaut)
<message "Au delà de 10, nous contacter pour une session privée (c'est pas plus cher !)">
**Cachés** 
Tél (vide)
Fuseau horaire (Europe/Paris GMT+1)
Langue (FR)
Devise (EUROS)
"Demande de devis" (go to 5.) sinon "Paiement direct" (go to 6.)
5. On est reconnu comme client, un devis est généré, **avec une convention de formation** et envoyé au client et a l'admin du site et un lien de téléchargement du devis est présenté sur le site FIN
6. On est reconnu comme client, on a le formulaire de paiement et on paye avec sa CB (go to 7.)
7. Le paiement est validé et une facture est générée et envoyée au client et a l'admin du site et un lien de téléchargement de la facture est présenté sur le site si paiement non valide, (go to 1.) FIN

### Correspondance avec support Stripe

- suite à l’édition via l’API d’un devis, est-ce que stripe envoie un email avec le devis ? 
- comment la personne valide le devis ? 
- lorsque le devis est accepté par le client, comment cela est il géré : Peut on envoyer directement le lien vers la page de paiement hébergée par stripe ? 
- la page de paiement a-t-elle une durée d'expiration ?

----

TO DO Florine ou Yann : 
    créer les 2 tarifs de toutes les formations dans stripe
    créer le modèle de convention avec les balises ou créer un modèle pour chaque session 
###### Références
https://stripe.com/docs/api/customers/create
https://stripe.com/docs/api/quotes/object

Le retour de Jordan : 
    À chaud :
Pourquoi demander :
Fuseau horaire (Europe/Paris GMT+1)
Langue (FR)
Devise (EUROS)
Pour le champs tel, je préciserais le code pays +33 à choisir avant le champs du tel
"Si paiement pas valide go to 1." Ça me semble retourné à une étape trop loin pour que la personne ait le courage de recommencer à remplir ses infos pour passer au paiement. Peut etre revenir direct sur l'étape ou toutes les informations CB sont préremplies avec le message d'erreur + possibilité de recliquer sur le bouton de paiement
Pour les questionnement, possibilité de faire valider un document en ligne avec une API DocHub ou autre service tiers qui permettent de faire de la signature électronique
service très possiblement payant
mais automatisé
Toute page de paiment a un délai avant expiration
Petite info, Stripe permet de créer deux autres type de paiement (facilement intégrable) :
plusieurs fois sans frais (classique)
période d'essai et prélévement par la suite
Je dis juste ça comme ça si ça peut vous intéresser pour intéresser d'autres personnes qui ont du mal à se lancer.