<?php
error_reporting(E_ALL);
require_once(__DIR__ . '/vendor/autoload.php');

if (file_exists('.env')) {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
} else {
    exit('ERROR : no .env file found, please copy .env.example to .env and change the values.');
}
if (empty($_ENV['PUBLIC_URL'])) {
    exit('ERROR : no value for PUBLIC_URL, edit the .env file and add the value.');
}
if (empty($_ENV['STRIPE_API_KEY'])) {
    exit('ERROR : no value for STRIPE_API_KEY, edit the .env file and add the value.');
}

require_once 'vendor/autoload.php';

// Enable debugging
phpCAS::setLogger();
// Enable verbose error messages. Disable in production!
phpCAS::setVerbose($_ENV['CAS_VERBOSE_MESSAGES']);

// Initialize phpCAS
phpCAS::client(
    CAS_VERSION_2_0,
    $_ENV['CAS_HOST'],
    (int) $_ENV['CAS_PORT'],
    $_ENV['CAS_CONTEXT']
);

// logout if desired
if (isset($_REQUEST['logout'])) {
    phpCAS::logout();
}

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);
// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();
// force CAS authentication
phpCAS::forceAuthentication();

# Stripe doc https://stripe.com/docs/api/php#intro
$stripe = new \Stripe\StripeClient($_ENV['STRIPE_API_KEY']);
$output = '';

if (!empty($_GET['purchase'])) {

} else {
    // if no purchases, show the products
    $products  = $stripe->products->all(['limit' => 3]);
    foreach ($products->data as $product) {
        $output .= '<article>';
        if (!empty($product->images[0])) {
            $output .= '<img style="float:left;margin-right:1em;max-width:230px;" src="'.$product->images[0].'" alt="image de présentation" />';
        }
        $output .= '<h3 class="card-title">'.$product->name.'</h3>'.$product->description;
        $output .= '<a role="button" href="'.$_ENV['PUBLIC_URL'].'?purchase='.$product->id.'">Passer commande</a>';
        //var_dump($product);
        $output .= '</article>';
    }
}

?>
<!doctype html>
<html lang="fr" data-theme="light">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/pico.min.css">
    <link rel="stylesheet" href="css/flexboxgrid.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title>UCPO - Stripe api POC</title>
  </head>
  <body>
      <main class="container">
    <nav style="margin-top: -4em;margin-bottom: 2em;">
        <ul>
            <li><strong>UCPO - Stripe api POC</strong></li>
        </ul>
        <ul>
            <li><?php echo phpCAS::getUser(); ?></li>
            <li><a href="<?php echo $_ENV['PUBLIC_URL']; ?>?logout">Se déconnecter</a></li>
        </ul>
    </nav>
        <?php echo $output; ?>
    </main>
  </body>
</html>